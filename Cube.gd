extends MeshInstance3D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var mat=null
var tex=null
var vectorTextureNode=null
# Called when the node enters the scene tree for the first time.
func _ready():
	mat=get_surface_override_material(0)
	vectorTextureNode=get_node("VectorTexture")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if vectorTextureNode.loaded()==false:
		var tex2= await vectorTextureNode.getTexture()
		if tex2!=null:
			mat.albedo_texture=tex2
