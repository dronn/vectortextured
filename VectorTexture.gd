extends "res://VectorPaint2.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
	


func _init(res=Vector2(1280, 720)):
	scaleFactor=1.5
	presetResolution=res


func getTexture():
	return await super.getTexture()

func loaded():
	return rerenderHistory==false

func clear():
	super.clear()

func _process(_delta):
	super._process(_delta)
	process_redraw()
