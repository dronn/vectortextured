extends Node3D
var material=null
func _ready():
	#get_node("Cube/VectorTexture").filetoLoad="res://sketch614_b.data"
	material = StandardMaterial3D.new()
	material.flags_transparent=false
	material.shading_mode=BaseMaterial3D.SHADING_MODE_UNSHADED
	var quader=get_node("Cube")
	quader.set_material_override(material)
	quader.mat=material
	
func clear():
	get_node("Cube/VectorTexture").clear()
