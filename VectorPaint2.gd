extends "res://vectorpaint/GDVectorPaint.gd"


var frameCounter=0
var fileLoaded=false
var filetoLoad="res://sketch614_b.data"

func _ready():
	super._ready()
	get_node("SubViewportContainer/SubViewport/Buffer").flipY=false
	get_node("SubViewportContainer/SubViewport/Buffer").clear()
	get_node("SubViewportContainer/SubViewport").transparent_bg=false

func _init(res=Vector2(1280, 720)):
	scaleFactor=1.5
	presetResolution=res

func clear():
	get_node("SubViewportContainer/SubViewport/Buffer").clear()

func _process(_delta):
	if frameCounter<3:
		frameCounter=frameCounter+1
		
	if frameCounter==2:
		loadBinary2(filetoLoad)
		fileLoaded=true

