extends Node3D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var quaders= []
var thread
var counter=1

func _init():
	#RenderingServer.set_default_clear_color(Color(1,1,1))
	pass

func _ready():
	$Timer.start(30)
	pass # Replace with function body.
	
func _thread_function(number):
	var quader=get_node("quader2").duplicate()
	quader.file="res://sketch614_b.data"
	quaders.append(quader)
	quader.visible=true
	quader.translate(Vector3(-10*number,0,0))
	add_child(quader)
	quader.clear()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func dir_contents(counter, path="res://sketches"):
	var actualCounter=counter
	var dir = DirAccess.open(path) 
	if dir.open(path) != null:
		dir.list_dir_begin() # TODOGODOT4 fill missing arguments https://github.com/godotengine/godot/pull/40547
		var file_name = dir.get_next()
		var i=0
		var file
		while file_name != "":
			if dir.current_is_dir():
				pass
				#print("Found directory: " + file_name)
			elif i==actualCounter:
				file="res://sketches/"+file_name
			
			file_name = dir.get_next()
			i=i+1
		
		if i< actualCounter:
			actualCounter=actualCounter%i
			i=0
			while file_name != "":
				if dir.current_is_dir():
					pass
					#print("Found directory: " + file_name)
				elif i==actualCounter:
					file="res://sketches/"+file_name
			
				file_name = dir.get_next()
				i=i+1
		print(file)
		if file==null:
			return "res://sketch267_b.data"
		return file
	else:
		print("An error occurred when trying to access the path.")
		return "res://sketch267_b.data"
		
func next():
	#thread=Thread.new()
	#thread.start(Callable(self,"_thread_function").bind(quaders.size()+1))
	var quader=get_node("quader2").duplicate()
	if quaders.size()>0:
		quaders[quaders.size()-1].visible=false
	else:
		get_node("quader2").visible=false
	
	#if OS.has_feature('JavaScript'):
	#	quader.file="res://sketch267_b.data"
	#else:
	quader.file=dir_contents(counter%90)
	counter=counter+1
	quaders.append(quader)
	quader.visible=true
	quader.translate(Vector3(-10*quaders.size(),0,0))
	add_child(quader)


func _input(ev):
	
	if Input.is_action_pressed("quit"):
		get_tree().quit()
	if ev is InputEventMouseMotion:
		pass
	elif Input.is_action_just_pressed("spawn") and ev.pressed  and not ev.is_echo():
		#thread=Thread.new()
		#thread.start(Callable(self,"_thread_function").bind(quaders.size()+1))
		var quader=get_node("quader2").duplicate()
		
		#if quaders.size()>0:
		#	quaders[quaders.size()-1].visible=false
		#else:
		#	get_node("quader2").visible=false
		quader.file=dir_contents(counter)
		counter=counter+1
		quaders.append(quader)
		quader.visible=true
		quader.translate(Vector3(-10*quaders.size(),0,0))
		quader.clear()
		add_child(quader)
		


func _on_Timer_timeout():
	#next()
	pass
