extends Node3D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.




func _input(ev):
	if Input.is_action_pressed("forward"):
		translate(Vector3(0,0,-0.2))

	if Input.is_action_pressed("left"):
		translate(Vector3(-0.2,0,0))

	if Input.is_action_pressed("backward"):
		translate(Vector3(0,0,0.2))

	if Input.is_action_pressed("right"):
		translate(Vector3(0.2,0,0))

	if Input.is_action_pressed("quit"):
		get_tree().quit()
		
	if Input.is_action_pressed("rotateLeft"):
		var rotation = 0.1*Input.get_action_strength("rotateLeft")
		rotate_object_local(Vector3(0,1,0), rotation)
	
	if Input.is_action_pressed("rotateRight"):
		var rotation = -0.1*Input.get_action_strength("rotateRight")
		rotate_object_local(Vector3(0,1,0), rotation)
	
	if Input.is_action_pressed("rotateUp"):
		var rotation = 0.03*Input.get_action_strength("rotateUp")
		get_node("CharacterBody3D/Camera3D").rotate_object_local(Vector3(1,0,0), rotation)
	
	if Input.is_action_pressed("rotateDown"):
		var rotation = 0.03*Input.get_action_strength("rotateDown")
		get_node("CharacterBody3D/Camera3D").rotate_object_local(Vector3(1,0,0), -rotation)
		
	if ev is InputEventMouseMotion:
		var mouseMotion = ev.relative/100
		rotate_object_local(Vector3(0,1,0),-mouseMotion.x)
		get_node("CharacterBody3D/Camera3D").rotate_object_local(Vector3(1,0,0), -mouseMotion.y)

